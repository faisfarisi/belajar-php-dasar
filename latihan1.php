<?php 
//array
// membuat array

$hari = array("senin","selasa","rabu");
$bulan = ["januari","februari","maret"];
$arr = [100, "teks", true];

//menammpilkan array
//versi debugging
var_dump($hari);
echo "<br>";
print_r($bulan);
echo "<br>";
// menampilkan 1 elemen pada array
// echo tidak dapat menampilkan array
//echo hanya dapat mencetak isi arraynya saja, mencetak 1 elemen pada array nya
echo $arr[0]; 
?>