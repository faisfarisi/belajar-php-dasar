
<?php 
//array associtiative
// perngetiannya adalah sebuah variabel yang bisa memiliki banyak nilai, dan pasangan antara key dan value 
// bedanya dengan array biasa adalah key nya adalah string yang kita buat sendiri

// kita sudah bisa menampilkan isi array menggunakan array associtiatif 
$mahasiswa = [
    [
    "nama"=>"fais al farisi",
    "nim"=> "E31211972",
    "email"=> "faisfarisi@gmail.com",
    "jurusan"=>"Teknologi informasi",
    "gambar"=>"abizar.jpg"

    ],
    [
        "nama"=>"hamdan mubarak",
        "nim"=> "A3121772",
        "email"=> "hamdan@gmail.com",
        "jurusan"=>"Teknologi industri pangan",
        "gambar"=>"jamil.jpg"
       
    ],

];
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Daftar Mahasiswa</title>
</head>
<body>
    <h1> Daftar Mahasiswa</h1>
<?php foreach ($mahasiswa as $mhs) : ?>
    <ul>
        <li>
            <img src="img/<?= $mhs["gambar"]; ?>">
        </li>
        <li> Nama :  <?php echo $mhs["nama"]; ?></li>
        <li> Nim :  <?php echo $mhs["nim"]; ?></li>
        <li> Email :<?php echo $mhs["email"]; ?></li>
        <li> Jurusan :<?php echo $mhs["jurusan"]; ?></li>
    </ul>
<?php  endforeach;?>

</body>
</html>