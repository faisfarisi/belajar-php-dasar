<?php 
$buku = [
    [
    "judul"=>"Kerinduan tanpa ada obat",
    "pengarang"=>"farisi fais",
    "tahun_terbit"=>"2023",
    "penerbit"=>"PT.cinta terdalam diam",
    "gambar"=>"Kerinduan tanpa ada obat"

],
[
    "judul"=>"cemburu",
    "pengarang"=>"pa'is",
    "tahun_terbit"=>"2022",
    "penerbit"=>"PT.sakek-tadek obet",
    "gambar"=>""

],
];

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Data buku diperpukaan</title>
</head>
<body>
    <h1>Data Buku</h1>
    <?php foreach ($buku as $bk) : ?>
        <ul>
           
            <li> Judul :  <?php echo $bk["judul"]; ?></li>
            <li> Pengarang :  <?php echo $bk["pengarang"]; ?></li>
            <li> Tahun Terbit :<?php echo $bk["tahun_terbit"]; ?></li>
            <li> Penerbit :<?php echo $bk["penerbit"]; ?></li>
        </ul>
    <?php endforeach; ?>
</body>
</html>